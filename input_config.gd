extends Node

const DEFAULT_PATH = "res://default_input_config.cfg"
const PATH = "user://input_config.cfg"

const DEFAULT_KEYBOARD_PROFILE = "Default keyboard"
const DEFAULT_GAMEPAD_PROFILE = "Default gamepad"
const DEFAULT_NAME_FOR_NEW_PROFILE = "Custom"
const CURRENT_SECTION_NAME = "Current"
const CURRENT_BINDINGS_NAME = "device-profile_bindings"

signal profile_list_changed(profile_index: int)

var connected_devices: Array[int]
# I put this code here, although it creates an object in the "global"
# scope, and it sort of improves performance, but
# readability is degraded because of side effects
@onready var CONFIG_FILE := ConfigFile.new()
@onready var device_profile_bindings: Dictionary = _get_device_profile_bindings()

func _ready():
	_set_connected_devices()
	Input.joy_connection_changed.connect(_on_device_list_updated)
	
	# Re-generating the default configuration file
	if CONFIG_FILE.load(DEFAULT_PATH) != OK or OS.is_debug_build():
		CONFIG_FILE.clear()
		var arr_of_actions = InputMap.get_actions()
		for action in arr_of_actions:
			var arr_of_events = InputMap.action_get_events(action)
			var keyboard_inputs = arr_of_events.filter(_filter_joypad_inputs_out)
			var joypad_inputs = arr_of_events.filter(_filter_keyboard_inputs_out)
			
			CONFIG_FILE.set_value(DEFAULT_KEYBOARD_PROFILE, action, keyboard_inputs)
			CONFIG_FILE.set_value(DEFAULT_GAMEPAD_PROFILE, action, joypad_inputs)
		CONFIG_FILE.save(DEFAULT_PATH)
	# Clear InputMap
	for action in InputMap.get_actions():
		for event in InputMap.action_get_events(action):
			InputMap.action_erase_event(action, event)
	
	load_profiles_of_all_devices()

# Returns profiles suitable for given device
func get_profiles(device: int) -> Array[String]:
	CONFIG_FILE.clear()
	var profiles: Array[String] = []
	var default_profile := _get_default_profile_name(device)
	if CONFIG_FILE.load(PATH) == OK:
		var sections := CONFIG_FILE.get_sections()
		for profile in sections:
			# Remove CURRENT_SECTION_NAME since it's not even a profile name
			if profile != CURRENT_SECTION_NAME:
				var action: String = CONFIG_FILE.get_section_keys(profile)[0]
				# Device property should exist on first InputEvent
				var event_list: Array[InputEvent] = CONFIG_FILE.get_value(profile, action)
				var event := event_list[0]
				if (event.device == 0) == (device == 0):
					profiles.append(profile)
	
	profiles.append(default_profile)
	return profiles

func get_inputs_of_device(device: int) -> Dictionary:
	var action_event_dict := {}
	for action in InputMap.get_actions():
		action_event_dict[action] = []
		for event in InputMap.action_get_events(action):
			if event.device == device:
				action_event_dict[action].append(event)
	return action_event_dict

func change_profile(profile: String, device: int):
	_set_device_profile_binding(profile, device)
	_load_single_device_profile(profile, device)

func save_changes_to_profile(profile: String, device: int):
	# Determine invalid profile names
	var invalid_profiles: Array[String]
	if device == 0:
		invalid_profiles = get_profiles(1)
	else:
		invalid_profiles = get_profiles(0)
	invalid_profiles.append_array([DEFAULT_GAMEPAD_PROFILE, DEFAULT_KEYBOARD_PROFILE, ""])
	var profile_name_is_invalid := profile in invalid_profiles
	
	# FIXME: Super stinky code duplication on lines +1 and +11
	if profile_name_is_invalid:
		profile = DEFAULT_NAME_FOR_NEW_PROFILE
		profile = _fix_profile_name_collision(_get_all_profile_names(), profile)
	CONFIG_FILE.load(PATH)
	CONFIG_FILE.erase_section(profile)
	var inputs := get_inputs_of_device(device)
	for action in inputs.keys():
		var event_list: Array[InputEvent] = inputs[action]
		CONFIG_FILE.set_value(profile, action, event_list)
	CONFIG_FILE.save(PATH)
	if profile_name_is_invalid:
		# Fired since we create new profile here
		profile_list_changed.emit(profile)
		change_profile(profile, device)

func load_profiles_of_all_devices():
	for device in connected_devices:
		var profile: String 
		if device_profile_bindings.has(device):
			profile = device_profile_bindings[device]
		else:
			profile = _get_default_profile_name(device)
		_set_device_profile_binding(profile, device)
		_load_single_device_profile(profile, device)

func replace_event_in_input_map(action: StringName, old_event: InputEvent, new_event: InputEvent):
	InputMap.action_erase_event(action, old_event)
	InputMap.action_add_event(action, new_event)

func add_event_in_input_map(action: StringName, new_event: InputEvent):
	InputMap.action_add_event(action, new_event)

func rename_profile(profile: String, new_profile_name: String):
	if profile == new_profile_name: return
	
	var profiles := _get_all_profile_names()
	var profile_index = profiles.find(profile)
	new_profile_name = _fix_profile_name_collision(profiles, new_profile_name)
	
	CONFIG_FILE.clear()
	CONFIG_FILE.load(PATH)
	for action in CONFIG_FILE.get_section_keys(profile):
		var event_list: Array[InputEvent] = []
		for event in CONFIG_FILE.get_value(profile, action):
			event_list.append(event)
		CONFIG_FILE.set_value(new_profile_name, action, event_list)
	CONFIG_FILE.save(PATH)
	
	if profile_index != -1:
		if not profile in [DEFAULT_GAMEPAD_PROFILE, DEFAULT_KEYBOARD_PROFILE]:
			delete_profile(profile, false)
		else:
			push_error("profile has invalid name")
			return
	else:
		push_error("profile not found")
		return
	profile_list_changed.emit(new_profile_name)

func delete_profile(profile: String, emit_event := true):
	var devices_bound_to_profile: Array[int] = []
	var default_profile: String
	for device in device_profile_bindings.keys():
		if device_profile_bindings[device] == profile:
			devices_bound_to_profile.append(device)
	for device in devices_bound_to_profile:
		default_profile = _get_default_profile_name(device)
		_set_device_profile_binding(default_profile, device)
	CONFIG_FILE.clear()
	CONFIG_FILE.load(PATH)
	CONFIG_FILE.erase_section(profile)
	CONFIG_FILE.save(PATH)
	if emit_event:
		profile_list_changed.emit(default_profile)

func _get_all_profile_names() -> Array[String]:
	var keyboard_profiles = get_profiles(0)
	var joypad_profiles := get_profiles(1)
	keyboard_profiles.append_array(joypad_profiles)
	return keyboard_profiles

func _set_device_profile_binding(profile: String, device: int):
	CONFIG_FILE.clear()
	CONFIG_FILE.load(PATH)
	device_profile_bindings[device] = profile
	CONFIG_FILE.set_value(CURRENT_SECTION_NAME, CURRENT_BINDINGS_NAME, device_profile_bindings)
	CONFIG_FILE.save(PATH)

func _set_connected_devices():
	var devices := Input.get_connected_joypads()
	# Prepend keyboard + mouse
	devices.push_front(0)
	connected_devices = devices

func _get_device_profile_bindings() -> Dictionary:
	if CONFIG_FILE.load(PATH) == OK:
		var bindings = CONFIG_FILE.get_value(CURRENT_SECTION_NAME, CURRENT_BINDINGS_NAME)
		if bindings != null:
			return CONFIG_FILE.get_value(CURRENT_SECTION_NAME, CURRENT_BINDINGS_NAME)
		else: return {}
	return {}

func _get_default_profile_name(device: int) -> String:
	if device == 0:
		return DEFAULT_KEYBOARD_PROFILE
	else:
		return DEFAULT_GAMEPAD_PROFILE

func _load_single_device_profile(profile: String, device: int):
	var default_profile := _get_default_profile_name(device)
	var profiles := get_profiles(device)
	if profile in profiles and profile != default_profile:
		CONFIG_FILE.load(PATH)
	else:
		_set_device_profile_binding(default_profile, device)
		profile = default_profile
		CONFIG_FILE.load(DEFAULT_PATH)
	# Unload old inputs
	for action in InputMap.get_actions():
		for event in InputMap.action_get_events(action):
			if event.device == device:
				InputMap.action_erase_event(action, event)
	var action_list := InputMap.get_actions()
	for action in CONFIG_FILE.get_section_keys(profile):
		for event in CONFIG_FILE.get_value(profile, action):
			event.device = device
			InputMap.action_add_event(action, event)

func _filter_keyboard_inputs_out(event) -> bool: 
	return (event is InputEventJoypadButton or event is InputEventJoypadMotion)

func _filter_joypad_inputs_out(event) -> bool: 
	return not _filter_keyboard_inputs_out(event)

func _fix_profile_name_collision(profiles: Array[String], new_profile_name: String) -> String:
	if new_profile_name in profiles:
		var current_index = new_profile_name.length() - 1
		var previous_value = new_profile_name.substr(current_index)
		var next_value = previous_value
		while str2var(next_value) is int:
			previous_value = next_value
			current_index -= 1
			next_value = new_profile_name.substr(current_index)
		
		var prefix := new_profile_name.substr(0, current_index + 1)
		var suffix = str2var(previous_value)
		if suffix is int:
			new_profile_name = prefix + str(suffix + 1)
		else:
			new_profile_name = prefix + str(1)
		return _fix_profile_name_collision(profiles, new_profile_name)
	else:
		return new_profile_name

func _on_device_list_updated(device: int):
	_set_connected_devices()
	_load_single_device_profile(device_profile_bindings[device], device)
