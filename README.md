# Main menu
Just the main menu. Made for [Godot 4.0.alpha12](https://downloads.tuxfamily.org/godotengine/4.0/alpha11/)

# TODO:

Features:
## Handling controller unplug:
1. Check if game is paused by unplugged device
2. if paused by unplugged device: unpause OR set other controller as "owner" of menu (too complex tbf)

Refactor refactor refactor refactor!

# Goals
The goal of this project is to create libre, high quality, sophisticated main menu, which includes (but not limited to) this features:

* Full keyboard / gamepad support.
Everything should be accesible without pointing devices (be it touchpad, touchscreen or mouse)
* Visual signaing for hotkeys and navigation. Players shouldn't guess predefined inputs (except maybe for directional inputs)
* [Being able to swap X and O for playstation-like gamepads](https://www.eurogamer.net/in-a-historic-move-playstation-5-swaps-x-and-circle-button-use-in-japan)
* RTL and i18n support is a long-term goal

# Q&A

> What are res://default_*_config.cfg files for? 

They are needed to ensure that a reset to the default state is possible.

> Why is it so ugly?

The goal was to provide *functional* UI. It's also just an example on how to use actual wrapper
