extends Node2D

var pause_menu := preload("./pause_menu.tscn")

func _unhandled_input(event):
	if event.is_action_pressed("ui_toggle_pause_menu"):
		var node = pause_menu.instantiate()
		node.device_id = event.device
		get_tree().get_root().add_child(node)
