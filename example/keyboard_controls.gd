extends VBoxContainer

var ControlsRowResource := preload("./control_row.tscn")
# id of device which opened this menu
var device_id: int
var current_profile: String

@onready var DeviceNameLabel: Label = $ProfilePickerContainer/DeviceNameLabel
@onready var ProfilePicker: OptionButton = $ProfilePickerContainer/ProfilePicker
@onready var ControlsContainer: VBoxContainer = $ScrollContainer/VBoxContainer

func _ready():
	DeviceNameLabel.text = str(device_id)
	ProfilePicker.clear()
	var initial_profile: String = InputConfig.device_profile_bindings[device_id]
	_update_profiles_for_profile_picker(initial_profile)
	InputConfig.profile_list_changed.connect(_update_profiles_for_profile_picker)
	_fill_controls_container()
	current_profile = ProfilePicker.text
	# Compensating for a lack of autofocus
	ProfilePicker.grab_focus()

func _exit_tree():
	InputConfig.load_profiles_of_all_devices()

func _on_profile_picker_item_selected(index: int):
	var profiles := InputConfig.get_profiles(device_id)
	current_profile = profiles[index]
	InputConfig.change_profile(profiles[index], device_id)
	_fill_controls_container()

func _update_profiles_for_profile_picker(profile_name: String):
	var profiles := InputConfig.get_profiles(device_id)
	var profile_id: int = profiles.find(profile_name)
	ProfilePicker.clear()
	for profile in profiles:
		ProfilePicker.add_item(profile)
	
	ProfilePicker.select(profile_id)

func _on_save_button_pressed():
	InputConfig.save_changes_to_profile(current_profile, device_id)

func _fill_controls_container():
	for child in ControlsContainer.get_children():
		child.queue_free()
	for action in InputConfig.get_inputs_of_device(device_id):
		var ControlsRowInstance := ControlsRowResource.instantiate()
		ControlsRowInstance.init(action, device_id)
		ControlsContainer.add_child(ControlsRowInstance)

func _on_line_edit_text_submitted(new_text: String):
	InputConfig.rename_profile(current_profile, new_text)


func _on_delete_profile_button_pressed():
	InputConfig.delete_profile(current_profile)
