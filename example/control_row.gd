extends HBoxContainer

var action_name: StringName
var device_id: int

@onready var ActionName: Label = $ActionName

func _ready():
	var initial_event_list: Array[InputEvent] = InputMap.action_get_events(action_name)
	ActionName.text = action_name
	for index in 2:
		if initial_event_list.size() <= index: return
		var event := initial_event_list[index]
		var new_button_instance := EventToggleButton.new()
		add_child(new_button_instance)
		new_button_instance.init(action_name, event)
		index += 1

func init(action_name: String, device_id: int):
	self.action_name = action_name
	self.device_id = device_id
