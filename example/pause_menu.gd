extends VBoxContainer

var SettingsMenu := preload("./SettingsMenu.tscn")
var device_id: int

func _ready():
	get_child(0).grab_focus()
	Input.joy_connection_changed.connect(_on_joy_connection_changed)
	get_tree().paused = true

func _unhandled_input(event):
	if event.is_action_pressed("ui_toggle_pause_menu"):
		get_tree().get_root().set_input_as_handled()

func _on_quit_pressed():
	get_tree().quit()

func _on_settings_pressed():
	var node = SettingsMenu.instantiate()
	node.device_id = device_id
	get_tree().get_root().add_child(node)

func _on_resume_pressed():
	self.queue_free()
	get_tree().paused = false

func _on_joy_connection_changed():
	if device_id not in Input.get_connected_joypads():
		push_error("Active joystick is disconnected")
		self.queue_free()
