extends TabContainer

var device_id: int
var controls := preload("res://example/controls_menu.tscn")

func _ready():
	var node := controls.instantiate()
	node.device_id = device_id
	self.add_child(node)

func _unhandled_input(event):
	if event.is_action_pressed("ui_toggle_pause_menu"):
		_close_menu()

func _close_menu():
	get_tree().get_root().set_input_as_handled()
	self.queue_free()
