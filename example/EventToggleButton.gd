class_name EventToggleButton extends Button

var current_event: InputEvent
var action_name: StringName

func _ready():
	self.toggled.connect(_on_button_toggled)
	self.toggle_mode = true
	set_process_input(false)

func init(action_name: StringName, initial_event: InputEvent):
	self.size_flags_horizontal = SIZE_EXPAND_FILL
	self.action_name = action_name
	_change_current_event(initial_event)

func _change_current_event(new_event: InputEvent):
	current_event = new_event
	self.text = current_event.as_text()

func _rebind_key(new_event):
	InputConfig.replace_event_in_input_map(action_name, current_event, new_event)
	_change_current_event(new_event)

func _on_button_toggled(button_pressed):
	if button_pressed:
		# TODO: Open modal and get first input
		# modal_portal.visible = true
		
		set_process_input(true)

func _input(event):
	if event is InputEventKey or event is InputEventJoypadButton:
		_remove_modal_and_set_config(event)

func _remove_modal_and_set_config(event):
	get_tree().get_root().set_input_as_handled()
	set_process_input(false)
	self.set_pressed(false)
	_rebind_key(event)
